# Linux

## Сеть / работа с сетью / networking / net / сетевые
```
ip -s link ls <имя адаптера> //текущее состояние интерфейса, статистика RX/TX, packets, bytes, errors, dropped, carrier, alternatives name adapter
ip link set ens33 down //выключить интерфейс ens33
ip link set ens33 up //включить интерфейс ens33
```


## Менеджер пакетов DPKG/APT
```
apt install <name> //установка пакета
apt-cache search <name> //поиск пакета
apt update //обновление кеша репозиториев
```




## Менеджер пакетов YUM/DNF
```
dnf install <name> //установка пакета
dnf reinstall <name> //переустановка пакета
dnf list //вывод имен всех доступных и установленных пакетов
dnf list installed //список установленных пакетов в системе
dnf remove <name> //удаление пакета
dnf autoremove //удаление всех пакетов, которые не используются другими приложениями
dnf info <name> //информация о пакете
dnf search <name> //поиск пакета по имени в репозитории
dnf repolist all //вывод списка всех репозиториев
dnf repo info <name> //информация о репозитории
dnf config-manager -set-disabled <name> //отключение репозитория 
dnf config-manager -set-enabled <name> //включение репозитория
dnf groupinstall <name_group> //установка всех пакетов из группы с заданным именем
dnf update //обновить все пакеты в системе
```
# Работа с прикладным ПО

## Java
```
Sudo def install java-11-openjdk //установка JavaSDK
java - version //проверка версии java
javac MyClass.java //компиляция
java MyClass //запуск программы
javadoc -d doc MyClass.java //документирование приложения
jar cfe app.jar MyClass MyClass.class Parsed.class //упаковывание приложения в jar архив



jar tf app.jar //просмотр содержимого архива jar
```
## NodeJS
```
curl -fsSL https://rpm.nodesource.com/setup_19.x | sudo bash - //добавление репозитория в систему (скачивание)
sudo def install nodejs //установка nodejs
node -- version //проверка версии nodejs
node app.js //запуск 
```

## Npm 
```
Ставится вместе с NodeJS автоматически
npm -- version //вывод версии npm
npm search <package> //поиск пакета
npm install <pakage> //установка пакета в текущий репозиторий
npm install <package> -g //установка пакета глобально на сервер
npm list -g //вывод глобально установленных пакетов в системе
```

## Python
```
python python2 python3 //проверка какая версия установлена в систему
sudo def install python3.11 //установка python3 версии 11
python3 //переход в интерпретатор python3
python -- version //вывод текущей версии
python3 main.py //запуск приложения
```

## Pip
```
pip -- version //проверка версии
sudo pip3 install <package> //глобальная установка пакета
pip3 install <packages> //установка пакета для текущего пользователя
pip3 show <package> //информация о пакете (ресположение пакета в системе)
python3 -m site //вывод информации о структуре Python в системе
Requirements.txt - файл для установки множества пакетов с определенной версией)
pip3 install -r requirements.txt //установить пакеты из файла requirements.txt
pip3 install flask -- upgrade //обновить пакет
pip3 uninstall <package> //удалить пакет
```


## GO
```
sudo dnf install go-toolset //установка golang (не самой последней версии)
go version //проверка версии
go help //вывод справочной информации по командам go
go run main.go //запуск программы без компиляции
go build main.go //создание бинарного файла программы (компиляция)
go env //вывод переменных сред golang
go mod init test //создание модуля проекта (программы)
GO1111MODULE = off //отключение использования модулей
```

## GIT + GitHub
```
dnf install git-all //установка git
git version //вывод версии git
git config --list //Вывод текущей конфигурации .gitconfig (лежит в /home/)
git config --global user.name “Andrey Deryugin” //задаем свое имя пользователя для указания в коммитах
git config --global user.email “aderugin@mail.ru” //задаем свой email для указания в коммитах
git config --global credential.helper cached //кеширование учетных данных (не нужно вручную вводить имя\email
git init //инициализация репозитория git
git status //текущее состояние репозитория (что отслеживается а что нет, что ожидается)
git add <files> //файлы отслеживаются (помещаются в область подготовленных файлов) и готовы к коммиту
git add --all // добавить в индекс все файлы данного репозитория
git commit -m “name commit” //создание коммита всего что отслеживалось ранее
git commit —amend -m “message” //внесение изменений в последний коммит 
git rm <file> //Удалить файл с добавлением изменения с индекс
git checkout <file> //вернуть состояние файла до всех текущих изменений к последнему коммиту
git log -p //просмотр истории коммитов с изменениями
git show 1af12……. //просмотр заданного коммита
git diff //просмотр изменений до коммита
git diff --staged //просмотр подготовленных изменений (которые добавлены в add)
git diff some file.js //просмотр изменений в конкретном файле
git revert HEAD //откат последнего коммита
git revert 1af17e //откат до заданного коммита
git branch <name> //создание новой ветки
git switch <name branch> || git checkout -b <name branch> //переход на другую ветку
git branch -a //вывод всех удаленных веток (и локальных)
git branch -d | -D <name branch> //удаление ветки (-D принудительное удаление ветки минуя все предупреждения)
git branch -m <name> //переименование названия текущей ветки
git push origin --delete <name branch> //удаление веток в удаленном репозитории gitHub
git merge <name-branch> //слияние веток (эту ветку с основной)
git log --graph --oneline --all //история коммитов в виде графика для текущей ветки
git remote -v //просмотр удаленных URL-адресов
git pull //получение изменений из удаленного репозитория
```

# APACHE2
```
sudo dnf install httpd httpd-tools | sudo apt install apache2 //установка apache2 на CentOs и Debian 
systemctl start httpd //запуск apache2 (httpd or apache2)
firewall-cmd --permanent --add-service=http //для CentOS включаем службу http (80)
firewall-cmd --permanent --add-service=https //для CentOS включаем службу https (443)
firewall-cmd --reload //для CentOS перезагружаем брандмауэер чтобы новые правила вступили в действие 
```
## Настройка Apache2 Ubuntu (+ VirtualHosts)
```
sudo mkdir /var/www/example.com //создаем директорию для хранения html контента
sudo chown -R $USER:$USER /var/www/example.com // назначаем права доступа user который сейчас в системе
sudo chmod -R 755 /var/www/example.com //владелец полные права, остальные чтение + запись
sudo vim /var/www/example.com/index.html // создаем выводимый контент
sudo vim /etc/apache2/sites-available/example.com.conf //создаем файл виртуального хоста
	<VirtualHost *:80>
		ServerAdmin admin@localhost
		ServerName example.com
		ServerAlias www.example.com
		DocumentRoot /var/www/example.com
		ErrorLog ${APACHE_LOG_DIR}/error.log
		CustomLog ${APACHE_LOG_DIR}/access.log combined
	</VirtualHost>
sudo a2ensite example.com.conf //включаем файл конфигурации
sudo a2dissite 000-default.conf //отключаем сайт по умолчанию
sudo apache2ctl configtest //проверяем наличие ошибок в конфигурационном файле
sudo systemctl restart apache2 //перезапускаем apache2 для внесения изменений

/etc/apache2 - каталог конфигурации Apache. Здесь все файлы конфигурации
/etc/apache2/apache2.conf - основной файл конфигурации Apache. Его редактировать для глобальных изменений. 
/etc/apache2/ports.conf - определяет порты которые будет слушать apache2. по умолчанию 80
/etc/apache2/sites-available/ - каталог, в котором могут храниться виртуальные хосты для каждого сайта. Apache не будет использовать файлы конфигурации, находящиеся в этом каталоге, но не связаны с этим sites-enabled каталогом. 
/etc/apache2/sites-enabled/ - каталог, в котором хранятся включенные виртуальные хосты для каждого сайта. Обычно это ссылка на файл из каталога sites-available с расширением a2ensite 
```

## Настройка Apache2 CentOS9 (+VirtualHosts)
```
sudo mkdir /var/www/example.com // создаем корневую папку для нашего виртуального хоста
sudo chown -R apache:apache /var/www/example.com // даем доступ пользователю apache
sudo chmod -R 755 /var/www/example.com //владелец все, остальные чтение+запись
sudo vim /var/www/example.com/index.html //создаем файл html
sudo mkdir /etc/httpd/sites-available /etc/httpd/sites-enabled // создаем каталоги для хранения конфигураций
sudo vim /etc/httpd/conf/httpd.conf // говорим Apache искать файлы виртуальных конфигураций в каталоге 
	IncludeOptional sites-enabled/*.conf
sudo vim /etc/httpd/sites-available/example.com.conf //пишем файл виртуального хоста
	<VirtualHost *:80>
		ServerAdmin admin@localhost
		ServerName example_domain
		ServerAlias www.example.com
		DocumentRoot /var/www/example.com
	</VirtualHost>
sudo vim /etc/httpd/conf/httpd.conf //изменяем права доступа к серверу 
	<Directory /var/www/example.com/>
	Options Indexes FollowSymLinks
	AllowOverride None
	Require all granted
	</Directory>
sudo ln -s /etc/httpd/sites-available/example.com.conf /etc/httpd/sites-enabled/ //включаем виртуальных хост 
sudo systemctl restart httpd  //вносим изменения в систему перезагрузкой службы apache2
```

## просмотр логов
```
journalctl -u httpd //все что относится к серверу httpd
cat /var/log/httpd/access_log //журнал доступа. Обновляется каждый раз когда пользователь получает доступ
cat /var/log/httpd/error_log //журнал ошибок. Обновляется при появлении ошибки
```
## Настройка конфигурации httpd
```
/etc/httpd/conf/httpd.conf //файл конфигурации httpd
DocumentRoot “/var/www/html” //место хранения статичного контента (веб страницы)
```

## Ansible
```
sudo dnf install ansible | sudo apt install ansible //Установка ansible 
sudo apt-add-repository papa:ansible/ansible //установка на Ubuntu
cat /etc/ansible/hosts //создать инвентарный файл

[servers]
Hostname1 ansible_host=192.168.55.55 ansible_user=andrey ansible_password=
Hostname2 ansible_host=192.168.55.56 ansible_user=andrey ansible_password=
…

```

## MySQL (openSource, SQL, надежная, масштабируемая, производительная, невысокая стоимость владения)
```
curl -L <адрес сайта>.rpm -O //скачиваем дистрибутив пакета 
dnf install MySQL80.rpm
dnf install MySQL-server
systemctl start mysqld | status mysqld //запускаем базу данных
/var/log/mysqld.log //лог базы данных хранится тут
mysql -uroot -p<password> //входим в утилиту управления базой данных
	ALTER USER “root”@“localhost” IDENTIFIED BY “password” //изменение первичного пароля от MySQL утилиты
	SHOW DATABASES; //вывод всех баз данных
	USE zoo; //входим в базу данных
	CREATE TABLE animals
	(
		nickname varchar(255),
		age int,
		 location varchar(255)
	); //создаем таблицу
	SHOW TABLES; //вывод всех таблиц
	INSERT INTO animals values (“Test”,11,”test2”); //вставка данных в таблицу
	SELECT * FROM animals; //вывод всех значений в таблице animals
	CREATE USER “max”@“localhost” IDENTIFIED BY “password” //создаем пользователя
	GRANT <permission> ON <DB.TABLE> TO ‘‘<username>@“%”; //назначение привилегий пользователю
	SHOW GRANT FOR “max”@“localhost”; //список всех разрешений для конкретного пользователя
```

## Vagrant
```
vagrant box list //вывод списка всех box
vagrant box remove <name> //удалить box
vagrant box add Ubuntu/focal64 <name.box> //добавление box в vagrant

```


## Поиск
```
grep -r “строка для поиска” //рекурсивный поиск по файлам во всех каталогах где ты находишься сейчас
grep -e “httpd” -e “tree” //множественный поиск (если нужно объединить выражение и осуществить поиск нескольких строк)
```

## Скачивание из интернета
```
curl -O https://….. //в случае обрыва соединения curl повторно попробует скачать файл (-O)
```

## Работа с архивами
```
tar -xvf <file.tar.gz> -C <directory destination> //распаковка в нужное место
```

## Просто команды (все)
```
last // вывод списка всех последних логинов на сервер
```








